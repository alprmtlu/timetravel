import { reactive, readonly, inject, App, watch, toRefs } from 'vue';
import axios from 'axios'
import { Post, Action } from '../models'

interface PostsState {
  ids: number[]
  all: Map<number, Post>
  orderedListById: Post[]
  isLoaded: boolean
}

interface ActionsState {
  ids: number[]
  all: Map<number, Action>
}

export interface State {
  posts: PostsState,
  actions: ActionsState
}

export const storeKey = Symbol('store')

export class Store {
  private state: State

  constructor(initial: State) {
    this.state = reactive(initial)
    this.watchPostList()
  }

  install(app: App) {
    app.provide(storeKey, this)
  }

  getState() {
    return readonly(this.state)
  }

  async fetchPosts() {
    const response = await axios.get<Post[]>('https://jsonplaceholder.typicode.com/posts')
    const newPostsState: PostsState = {
      ids: [],
      all: new Map,
      orderedListById: [],
      isLoaded: true
    }
    const firstFivePosts = response.data.reduce<Post[]>((acc, post) => {
      if (post.id < 0 || post.id > 5) return acc

      return acc.concat(post)
    }, [])

    for (const post of firstFivePosts) {
      newPostsState.ids.push(post.id)
      newPostsState.all.set(post.id, post)
      newPostsState.orderedListById.push(post)
    }

    this.state.posts = newPostsState
  }

  watchPostList() {
    watch(
      () => this.state.posts.ids,
      (newIdList: number[]) => {
        const orderedPosts: Post[] = newIdList.reduce<Post[]>((acc, id) => {
          const thePost = this.state.posts.all.get(id)

          if(!thePost) {
            throw Error('This post was not found')
          }

          return acc.concat(thePost)
        }, [])

        this.state.posts.orderedListById = orderedPosts
      },
      { deep: true }
    )
  }

  /**
   * NOTE: Array.from is used instead of spread operator due to lack of tslib support at this version.
   */
  changePosition(postId: number, currentPos: number, newPos: number) {
    const tempIds = Array.from(this.state.posts.ids)
    const currentPostIds = Array.from(tempIds)
    const replacedPostId = tempIds[newPos]

    tempIds[newPos] = postId
    tempIds[currentPos] = replacedPostId

    this.saveTravel(postId, currentPos, newPos, currentPostIds)
    this.state.posts.ids = tempIds
  }

  private saveTravel(postId: number, currentPos: number, newPos: number, positionList: number[]) {
    const { ids } = toRefs(this.state.actions)
    const idsRef = ids.value
    const lastActionId = idsRef.length ? idsRef[0] : 0
    const newActionId = lastActionId + 1
    const newAction: Action = {
      id: newActionId,
      postId,
      oldPosition: currentPos,
      newPosition: newPos,
      positionList
    }

    idsRef.unshift(newActionId)
    this.state.actions.all.set(newActionId, newAction)
  }

  handleTimeTravel(action: Action) {
    const newActionsState: ActionsState = {
      ids: [],
      all: new Map
    }

    // update posts
    this.state.posts.ids = action.positionList

    // remove the selected time travel and above
    const newActionIds = this.state.actions.ids.filter((id) => id < action.id)

    this.state.actions.all.forEach((value, key) => {
      newActionIds.includes(key) && newActionsState.all.set(key, value)
    })

    newActionsState.ids = newActionIds
    this.state.actions = newActionsState
  }
}

export const store = new Store({
  posts: {
    all: new Map(),
    ids: [],
    orderedListById: [],
    isLoaded: false
  },
  actions: {
    all: new Map(),
    ids: []
  }
})

export function useStore(): Store {
  const _store = inject<Store>(storeKey)

  if(!_store) {
    throw Error('Did you forget to call provide?')
  }

  return _store
}