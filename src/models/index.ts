interface Post {
  id: number
  userId: number
  title: string
  body: string
}

interface Action {
  id: number
  postId: number
  oldPosition: number
  newPosition: number
  positionList: number[]
}

export {
  Post,
  Action
}