import { mount } from '@vue/test-utils'
import { postsData } from '../../src/mocks'
import { Store, State } from '../../src/store'
import Post from '@/components/Post.vue'
import { Post as IPost } from '@/models'

function setStore(): Store {
  return new Store({
    posts: {
      all: new Map([
        [1, postsData[0]],
        [2, postsData[1]],
        [3, postsData[2]],
        [4, postsData[3]],
        [5, postsData[4]],
      ]),
      ids: [1, 2, 3, 4, 5],
      orderedListById: postsData,
      isLoaded: true
    },
    actions: {
      all: new Map(),
      ids: []
    }
  })
}

let store = setStore()

function mountPost(post: IPost, index: number) {
  return mount(Post, {
    props: {
      post,
      index
    },
    global: {
      plugins: [store]
    }
  })
}

describe('Post.vue', () => {

  beforeEach(() => {
    store = setStore()
  })

  it('renders the post context correctly', () => {
    const wrapper = mountPost(postsData[0], 0)

    expect(wrapper.find('.post__title').html()).toContain(postsData[0].title)
    expect(wrapper.find('.post__body').html()).toContain(postsData[0].body)
  })

  it('only shows the bottom button when it is the first post', () => {
    const wrapper = mountPost(postsData[0], 0)

    expect(wrapper.find('.post__btn--top').exists()).toBeFalsy()
    expect(wrapper.find('.post__btn--bottom').exists()).toBeTruthy()
  })

  it('only shows the up button when it is the last post', () => {
    const wrapper = mountPost(postsData[4], 4)

    expect(wrapper.find('.post__btn--top').exists()).toBeTruthy()
    expect(wrapper.find('.post__btn--bottom').exists()).toBeFalsy()
  })

  it('updates the state when down button is clicked', async () => {
    const wrapper = mountPost(postsData[0], 0)

    await wrapper.get('.post__btn--bottom').trigger('click')

    expect( store.getState().posts.orderedListById[0].id ).toEqual(postsData[1].id)
    expect( store.getState().posts.orderedListById[1].id ).toEqual(postsData[0].id)
    expect( store.getState().posts.orderedListById[2].id ).toEqual(postsData[2].id)
  })

  it('updates the state when up button is clicked', async () => {
    const wrapper = mountPost(postsData[2], 2)

    await wrapper.get('.post__btn--top').trigger('click')

    expect( store.getState().posts.orderedListById[0].id ).toEqual(postsData[0].id)
    expect( store.getState().posts.orderedListById[1].id ).toEqual(postsData[2].id)
    expect( store.getState().posts.orderedListById[2].id ).toEqual(postsData[1].id)
  })

})