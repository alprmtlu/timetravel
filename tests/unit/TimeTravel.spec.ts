import { mount } from '@vue/test-utils'
import { actionsData, postsData } from '../../src/mocks'
import { Store } from '../../src/store'
import TimeTravel from '@/components/TimeTravel.vue'
import { Action } from '@/models'

function setStore(): Store {
  return new Store({
    posts: {
      all: new Map([
        [1, postsData[0]],
        [2, postsData[1]],
        [3, postsData[2]],
        [4, postsData[3]],
        [5, postsData[4]],
      ]),
      ids: [1, 2, 3, 4, 5],
      orderedListById: postsData,
      isLoaded: true
    },
    actions: {
      all: new Map([
        [1, actionsData[0]],
        [2, actionsData[1]],
        [3, actionsData[2]],
      ]),
      ids: [3, 2, 1]
    }
  })
}

let store = setStore()

function mountTravel(action: Action) {
  return mount(TimeTravel, {
    props: {
      action
    },
    global: {
      plugins: [store]
    }
  })
}

describe('TimeTravel.vue', () => {

  beforeEach(() => {
    store = setStore()
  })

  it('renders the travel list correctly', () => {
    const wrapper = mountTravel(actionsData[0])

    expect(wrapper.find('.travel__detail').html()).toContain(
      `Moved post ${actionsData[0].postId} from index ${actionsData[0].oldPosition} to index ${actionsData[0].newPosition}`
      )
    expect(wrapper.find('.travel__btn').exists()).toBeTruthy()
  })

  it('updates the state when the first time travel button is clicked', async () => {
    const wrapper = mountTravel(actionsData[0])

    await wrapper.get('.travel__btn').trigger('click')

    expect( store.getState().actions.all.size ).toEqual(0)
    expect( store.getState().actions.ids.length ).toEqual(0)
  })

  it('updates the state when the time travel button in the middle is clicked', async () => {
    const wrapper = mountTravel(actionsData[1])

    await wrapper.get('.travel__btn').trigger('click')

    expect( store.getState().actions.all.size ).toEqual(1)
    expect( store.getState().actions.ids.length ).toEqual(1)
    expect( store.getState().actions.ids ).toEqual([1])
  })

  it('updates the posts.orderedListById state when the time travel button is clicked', async () => {
    const wrapper = mountTravel(actionsData[1])

    await wrapper.get('.travel__btn').trigger('click')
    const newOrderedList = Array.from(store.getState().posts.orderedListById)

    expect( newOrderedList[0].id ).toEqual(2)
    expect( newOrderedList[1].id ).toEqual(1)
    expect( newOrderedList[2].id ).toEqual(3)
    expect( newOrderedList[3].id ).toEqual(4)
    expect( newOrderedList[4].id ).toEqual(5)
  })

})