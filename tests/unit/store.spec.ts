import { postData } from '../../src/mocks'
import { State, Store } from '../../src/store'

jest.mock('axios', () => ({
  get: (url: string) => {
    return Promise.resolve({
      data: [ postData ]
    })
  }
}))

describe('store/fetchPosts', () => {

  it('fetches post and updates state', async () => {
    const store = new Store({
      posts: {
        all: new Map(),
        ids: [],
        orderedListById: [],
        isLoaded: false
      },
      actions: {
        all: new Map(),
        ids: []
      }
    })

    await store.fetchPosts()

    const expected: State = {
      posts: {
        all: new Map([[1, postData]]),
        ids: [1],
        orderedListById: [postData],
        isLoaded: true
      },
      actions: {
        all: new Map(),
        ids: []
      }
    }

    expect(expected).toEqual(store.getState())
  })

})