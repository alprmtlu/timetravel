import { flushPromises, mount } from '@vue/test-utils'
import Home from '@/components/Home.vue'
import { postsData } from '../../src/mocks'
import { Store } from '../../src/store'

jest.mock('axios', () => ({
  get: (url: string) => {
    return Promise.resolve({
      data: postsData
    })
  }
}))

function mountHome() {
  const store = new Store({
    posts: {
      all: new Map(),
      ids: [],
      orderedListById: [],
      isLoaded: false
    },
    actions: {
      all: new Map(),
      ids: []
    }
  })

  const testComp = {
    components: { Home },
    template: `
      <suspense>
        <template #default>
          <Home />
        </template>
        <template #fallback>
          Loading...
        </template>
      </suspense>
    `
  }

  return mount(testComp, {
    global: {
      plugins: [store]
    }
  })
}

describe('Home.vue', () => {

  it('renders page correctly', async () => {
    const wrapper = mountHome()
    await flushPromises()

    expect(wrapper.findAll('.post')).toHaveLength(5)
    expect(wrapper.findAll('.travel__item')).toHaveLength(1)
    expect(wrapper.find('.travel__detail').text()).toBe('No action has been committed yet')
    expect(wrapper.html()).toContain(postsData[0].body)
    expect(wrapper.html()).toContain(postsData[0].title)
    expect(wrapper.findAll('.post')[0].html()).not.toContain('post__btn--top')
    expect(wrapper.findAll('.post')[1].html()).toContain('post__btn--top')
    expect(wrapper.findAll('.post')[1].html()).toContain('post__btn--bottom')
    expect(wrapper.findAll('.post')[4].html()).not.toContain('post__btn--bottom')
  })

  it('moves down the post when click the down button', async () => {
    const wrapper = mountHome()

    await flushPromises()
    await wrapper.get('.post').find('.post__btn--bottom').trigger('click')

    expect(wrapper.findAll('.post')[0].get('.post__title').text()).toMatch(postsData[1].title)
    expect(wrapper.findAll('.post')[1].get('.post__title').text()).toMatch(postsData[0].title)
  })

  it('moves up the post when click the up button', async () => {
    const wrapper = mountHome()

    await flushPromises()
    await wrapper.findAll('.post')[1].find('.post__btn--top').trigger('click')

    expect(wrapper.findAll('.post')[0].get('.post__title').text()).toMatch(postsData[1].title)
    expect(wrapper.findAll('.post')[1].get('.post__title').text()).toMatch(postsData[0].title)
  })

  it('creates new time travel when click the down button', async () => {
    const wrapper = mountHome()

    await flushPromises()
    await wrapper.get('.post').find('.post__btn--bottom').trigger('click')

    expect(wrapper.findAll('.travel__item')[0].get('.travel__detail').text()).toMatch('Moved post 1 from index 0 to index 1')
  })

  it('creates new time travel when click the up button', async () => {
    const wrapper = mountHome()

    await flushPromises()
    await wrapper.findAll('.post')[1].find('.post__btn--top').trigger('click')

    expect(wrapper.findAll('.travel__item')[0].get('.travel__detail').text()).toMatch('Moved post 2 from index 1 to index 0')
  })

  it('deletes the time travel list item and updates the posts list when click the time travel button', async () => {
    const wrapper = mountHome()

    await flushPromises()
    await wrapper.findAll('.post')[1].find('.post__btn--top').trigger('click')

    expect(wrapper.findAll('.travel__item')[0].get('.travel__detail').text()).toMatch('Moved post 2 from index 1 to index 0')

    await wrapper.get('.travel__btn').trigger('click')

    expect(wrapper.find('.travel__detail').text()).toBe('No action has been committed yet')
    expect(wrapper.findAll('.post')[0].html()).toContain(postsData[0].title)
    expect(wrapper.findAll('.post')[1].html()).toContain(postsData[1].title)
  })

})